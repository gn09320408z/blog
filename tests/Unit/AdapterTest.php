<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Test\Userlist;
use App\Test\ShipAdapter;
use App\Test\Ship;

class AdapterTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

     public function testgetFee(){


        $Userlist = new Userlist();

        $ShipAdapter = new ShipAdapter($Userlist);

        $Ship = new Ship($ShipAdapter);
        
        
        
        $this->assertSame(110,$Ship->getFee());

    }
}
