@extends('main')

@section('title', "| $posts->title")

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<br>
			<h3>標題：{{ $posts->title }}</h3>
			@if($posts->category)
			<h6>分類：{{ $posts->category->name }}</h6>
			@endif
			<br>
			<p>{!! $posts->body !!}</p>						
			<hr>
		</div>		
	</div>
	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3 class="comment-title"><span class="Chat square fill"><svg class="bi bi-chat-square-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  			<path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h2.5a1 1 0 0 1 .8.4l1.9 2.533a1 1 0 0 0 1.6 0l1.9-2.533a1 1 0 0 1 .8-.4H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
			</svg></span>{{ $posts->comments()->count() }}則留言：</h3>
			@foreach($posts->comments as $comment)
			<div class="comment">
				<div class="author-info">
					<img src="{{ "https://www.gravatar.com/avatar/" . md5(strtolower(trim($comment->email))) . "?s=50&d=identicon" }} " class="author-image">
					<div class="author-name">
						<h4>{{ $comment->name }}</h4>
						<p class="author-time">{{ date('F nS, Y - g:ia ',strtotime($comment->created_at)) }}</p>
					</div>					
				</div>
				<div class="comment-content">
					{{ $comment->comment }}
				</div>
			</div>					
			@endforeach
		</div>
	</div>
	<br><br>
	<div class="row">
		<div id="comment-form" class="col-md-8 col-md-offset-2" style="margin-top: 50px;">
			{{ Form::open(['route' => ['comments.store' , $posts->id] , 'method' => 'POST'  ]) }}

			<div class="row">
				<div class="col-md-6">
					{{ Form::label('name' , "Name:") }}
					{{ Form::text('name' , null , ['class' => 'form-control']) }}
				</div>

				<div class="col-md-6">
					{{ Form::label('email' , "Email:") }}
					{{ Form::text('email' , null, ['class' => 'form-control']) }}
				</div>

				<div class="col-md-12">
					{{ Form::label('comment' , "Comment:") }}
					{{ Form::textarea('comment' , null, ['class' => 'form-control']) }}
					<br>
					{{ Form::submit('新增留言' , ['class' => 'btn btn-success ']) }}
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>


@endsection