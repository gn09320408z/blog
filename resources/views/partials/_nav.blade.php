<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Blog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="{{ Request::is('/') ? "active" : "" }}"><a class="nav-link" href="/">首頁</a></li>
      <li class="{{ Request::is('about') ? "active" : "" }}"><a class="nav-link" href="/about">關於</a></li>
      <li class="{{ Request::is('contact') ? "active" : "" }}"><a class="nav-link" href="/contact">聯絡</a></li>
    </ul>
      <ul class="navbar-nav nav-item dropdown ml-auto">
        @if(Auth::check())
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ Auth::user()->email }}<span class="caret"></span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{ route('posts.index') }}">我的貼文</a>
          <a class="dropdown-item" href="{{ route('posts.create') }}">新增貼文</a>
          <a class="dropdown-item" href="{{ route('tags.index') }}">Tags</a>
          <a class="dropdown-item" href="{{ route('categories.index') }}">categroies</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/logout">登出</a>
        </div>
        @else
        <li><a class="nav-link" href="/login">登入</a></li> 
        @endif
      </ul>
  </div>
</nav>