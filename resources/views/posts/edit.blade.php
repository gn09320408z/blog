@extends('main')

@section('title', '| Edit Blog Post' )

@section('content')

@section('stylesheets')

	{{ Html::style('css/select2.min.css') }}

	<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

	<script>
		tinymce.init({
			selector: "textarea", 
			menubar: false ,
			plugins: 'link',
			toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link'
		});
	</script>

@endsection


	<div class="row">

		
		
		<div class="col-md-8">
			{!! Form::model($post, ['route' => ['posts.update',$post->id ] , 'method' => 'PUT'] ) !!}
			
			<br>
			{{ Form::label('title', 'Title:') }}
			{{ Form::text('title',null,["class" => 'form-control input-lg']) }}

			<br>
			{{ Form::label('slug', 'Slug:') }}
			{{ Form::text('slug',null,["class" => 'form-control input-lg']) }}

			<br>
			{{ Form::label('category_id' , 'Category:') }}
			{{ Form::select('category_id' , $categories, null , ['class' => 'form-control']) }}

			<br>	
			{{ Form::label('tags' , 'Tags:') }}
			{{ Form::select('tags[]' , $tags, null , ['class' => 'form-control select2-multi' ,'multiple' => 'multiple']) }}

			<br>
			{{ Form::label('body', 'Body:', ['class' => 'form-spacing-top']) }}
			{{ Form::textarea('body', null,['class'=> 'form-control ']) }}
			
			

		
		</div>
		
		<div class="col-md-4">
			
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Created At:</dt>
					<dd>{{ date( 'Y-m-j  H:ia', strtotime($post->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date( 'Y-m-j  H:ia', strtotime($post->updated_at)) }}</dd>
				</dl>

				<hr>
				

				 <div class="row">
				 	<div class="col-sm-4">
				 		{{ Form::submit('儲存',['class' => 'btn btn-success btn-block btn-sm']) }}
				 		
				 	</div>
					
					<div class="col-sm-4">
				 		{!! Html::linkRoute('posts.show','取消', array($post->id) , array('class' => 'btn btn-danger btn-block btn-sm')) !!}
				 	</div>
				 				 	

				 </div>

			</div>
			{!! Form::close() !!}
		</div>
		
	</div>

	

@endsection

@section('scripts')


	{{ Html::style('js/select2.min.js') }}
	<script type="text/javascript">
		$('.select2-multi').select2();
	</script>

@endsection

