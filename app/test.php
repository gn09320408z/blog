<?php
namespace App;

/*
    totalfee 總運費
    basicfee 基本運費
    mincount 最小計費公斤數
    totalkg 總公斤
    freekg 免費公斤數
    equalkgfee 上限公斤費用
    perkg 公斤為單位
    feeperunit 每單位價格
    
    formula => $totalfee = $basicfee + ceil((max($mincount, $totalkg - $freekg)+($totalkg - $freekg==$equalkgfee))/$perkg) * $feeperunit ;

    deliver location weight
    Dog       US   0 0 X 0 0 1 60
    Falcon    CN 200 0 X 0 0 1 20
    Falcon    TW 150 0 X 5 1 1 30 
    Cat       TW 100 0 X 0 0 3 10
*/

    interface Usermethod{
        public function getDeliver();
        public function getLocation();
        public function getWeight();
    }
    interface Adapter{
        public function getParameter();
        public function FeeCal();
    }
    class Userlist implements Usermethod{
        protected $deliver;
        protected $location;
        protected $weight;

        public function getDeliver($deliver){
            return $this->deliver;
        }
        public function getLocation($location){
            return $this->location;
        }
        public function getWeight($weight){
            return $this->weight;
        }
    }
    class Ship implements Adapter{
        private $ship;
        public function __construct($ship){
            $this->ship = $ship;
        }
        public function getFee(){
            return $this->ship->FeeCal($basicfee,$mincount,$totalkg,$freekg,$equalkgfee,$perkg,$feeperunit);
        }
    }
    class ShipAdapter implements Usermethod{
        protected $totalfee;
        protected $basicfee;
        protected $mincount=0;
        protected $totalkg;
        protected $freekg=0;
        protected $equalkgfee=0;
        protected $perkg=1;
        protected $feeperunit;

        private $shipper;
        public function __construct(Userlist $userlist){
            $this->shipper = $userlist;
            $totalkg = $userlist->weight;
        }
        
        public function FeeCal(){
            if($shipper->deliver == 'Dog'){
                $basicfee = 0;
                $feeperunit = 60;
            }
            if($shipper->deliver == 'Falcon' && $shipper->location == 'CN'){
                $basicfee = 200;
                $feeperunit = 20;
            }
            if($shipper->deliver == 'Falcon' && $shipper->location == 'TW'){
                $basicfee = 150;
                $freekg = 5;
                $equalkgfee = 1;
                $feeperunit = 30;
            }
            if($shipper->deliver == 'Cat'){
                $basicfee = 100;
                $perkg = 3;
                $feeperunit = 10;
            }
            $totalfee = $basicfee + ceil((max($mincount, $totalkg - $freekg)+($totalkg - $freekg==$equalkgfee))/$perkg) * $feeperunit ;
            return $totalfee;
        }
    }
    
    /*
    public function testgetFee($deliver,$location,$weight){

        $Userlist = new Userlist;
        $ShipAdapter = new ShipAdapter($Userlist);
        $Ship = new Ship($ShipAdapter);

        $Ship->getFee($basicfee,$mincount,$totalkg,$freekg,$equalkgfee,$perkg,$feeperunit);



        return $this->getFee($totalfee);
    }
    */