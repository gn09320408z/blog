<?php

namespace App\Test;

use App\Test\Usermethod;

class ShipAdapter implements Usermethod {

		protected $userlist;
		private $deliver;
		private $location;
		private $weight;

        public function __construct($userlist){
            $this->userlist = $userlist;
        }
        public function getDeliver(){
        	return $this->userlist->getDeliver();
        }
        public function getLocation(){
        	return $this->userlist->getLocation();
        }
        public function getWeight(){
        	return $this->userlist->getWeight();
        }
        
        public function FeeCal():int{
        	
        	$totalfee;
	        $basicfee;
	        $mincount=0;
	        $totalkg = $this->userlist->getWeight();
	        $freekg=0;
	        $perkg=1;
	        $feeperunit;

            if($this->userlist->getDeliver() == "Dog"){
                $basicfee = 0;
                $feeperunit = 60;
            }
            if($this->userlist->getDeliver() == 'Falcon' && $this->userlist->getLocation() == 'CN'){
                $basicfee = 200;
                $feeperunit = 20;
            }
            if($this->userlist->getDeliver() == 'Falcon' && $this->userlist->getLocation() == 'TW'){
                $basicfee = 150;
                $freekg = 5;
                $feeperunit = 30;
            }
            if($this->userlist->getDeliver() == 'Cat'){
                $basicfee = 100;
                $perkg = 3;
                $feeperunit = 10;
            }
            $totalfee = $basicfee + ceil((max($mincount, $totalkg - $freekg)+($totalkg - $freekg==0))/$perkg) * $feeperunit ;
            return $totalfee;
        }
    }