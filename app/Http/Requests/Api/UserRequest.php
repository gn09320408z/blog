<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            {
                return[
                    'id' => ['required,exists:shop_user,id']
                ];
            }
            case 'POST':
            {
                return[
                    'name' => ['required','max:12','unique:users,name'],
                    'password' => ['required','max:16','min:6']
                ];
            }
            case 'PUT':
            case 'PATCH':
            case 'DELETE':
            default:
            {
                return [
                    
                ];
            }
        }
    }

    public function message(){
        return [
            'id.required'=>'用戶id需填寫',
            'id.exists'=>'用户不存在',
            'name.unique' => '用戶名已經存在',
            'name.required' => '用户名不能為空',
            'name.max' => '用戶名最大長度為12個字符',
            'password.required' => '密碼不能為空',
            'password.max' => '密碼長度不能超過16個字符',
            'password.min' => '密碼長度不能小於6個字符'
        ];
    }
}
