<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\UserRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    //
    public function getIndex(){
    	return response()->json([User::all()]);
    }
    public function store(UserRequest $request){
    	User::create([
    		'name' => $request->name,
    		'password' => Hash::make($request->password),
    		'email' => $request->email
    	]);
    	return response()->json(['message' => '用戶註冊成功']);
    }
    public function login(Request $request){
    	$res=Auth::guard('web')->attempt([
    		'email' => $request->email,
    		'password' => $request->password
    	]);
        
    	if($res){
    		return response()->json([
                'message' => '登入成功',
                'email' => $request->email,
                'password' => $request->password,
                'time' => date('Y-m-d h:i a',time()+8*3600)
            ]);
    	}
        
    	return response()->json([
                'message' => '登入失敗',
            ]);;
        
    }
}

