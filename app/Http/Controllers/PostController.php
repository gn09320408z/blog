<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use App\Tag;
use Session;
use Mews\Purifier\Facades\Purifier;
use App\User;
use Auth;
use Exception;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->name==='admin'){
            if ($request->title) {
                $posts = Post::where('title', 'like', '%' . $request->title . '%')->get();
                return view('posts.index',['posts'=>$posts]);
            } else {
                $posts = Post::orderBy('id','desc')->paginate(10);
                return view('posts.index')->withPosts($posts);
            }
        }else{
            if ($request->title) {
                $posts = Post::where('title', 'like', '%' . $request->title . '%')->where('username',Auth::user()->name)->get();
                return view('posts.index',['posts'=>$posts]);
            }else{
                $posts = Post::where('username',Auth::user()->name)->orderBy('id','desc')->get();
                return view('posts.index')->withPosts($posts);
            }
        }
        
            
    }

   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        $username = Auth::user()->name;
        return view('posts.create')->withCategories($categories)->withTags($tags)->withUsername($username);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user,Request $request)
    {


        // validate the data
        $this->validate($request,array(
            'title' => 'required|max:255',
            'slug' => 'required|alpha_dash',
            'category_id' => 'required|integer',
            'body' => 'required'

        ));

        // store in database

        $post = new Post;

        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->category_id = $request->category_id;
        $post->body = Purifier::clean($request->body);
        $post->username = Auth::user()->name;


        $post->save();

        $post->tags()->sync($request->tags, false);
        Session::flash('success','The blog post was successfully save!');

        // redirect to another page

        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        
        return view('posts.show')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        $categories = Category::all();
        
        $cats = array();
        foreach($categories as $category){
            $cats[$category->id] = $category->name;
        }

        $tags = Tag::all();
        $tags2 = array();
        foreach ($tags as $tag) {
            $tags2[$tag->id] = $tag->name;
        }

        return view('posts.edit')->withPost($post)->withCategories($cats)->withTags($tags2);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $post = Post::find($id);
        if ($request->input('slug') == $post->slug) {
            $this->validate($request,array(
            'title' => 'required|max:255',
            'category_id' => 'required|integer',
            'body' => 'required'
            ));   
        }else{
            $this->validate($request,array(
                'title' => 'required|max:255',
                'slug' => 'required|alpha_dash|unique:posts,slug',
                'category_id' => 'required|integer',
                'body' => 'required'
            ));
        }
        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->slug = $request->input('slug');
        $post->category_id = $request->input('category_id');
        $post->body = Purifier::clean($request->input('body'));
        $post->username = Auth::user()->name;

        $post->save();
        $post->tags()->sync($request->tags);
        Session::flash('success','Post was successfully saved!');

        return redirect()->route('posts.show', $post->id );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);
        $post->tags()->detach();
        
        $post->delete();

        Session::flash('success', 'The post was successfully deleted.');
        return redirect()->route('posts.index');
    }
}
